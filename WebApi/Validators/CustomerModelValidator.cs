﻿using FluentValidation;
using System;
using WebApi.Core.Models;

namespace WebApi.Validators
{
    public class CustomerModelValidator : AbstractValidator<CustomerModel>
    {
        public CustomerModelValidator()
        {
            // Standard rules
            RuleFor(m => m.PolicyHolderFirstName).NotEmpty().MinimumLength(3).MaximumLength(50);
            RuleFor(m => m.PolicyHolderLastName).NotEmpty().MinimumLength(3).MaximumLength(50);
            RuleFor(m => m.PolicyHolderReferenceNumber).NotEmpty().Matches("^[A-Z]{2}-[\\d]{6}$");

            // Dependent questions
            When(m => !m.PolicyHolderDob.HasValue && string.IsNullOrWhiteSpace(m.PolicyHolderEmailAddress), () =>
            {
                RuleFor(m => m.PolicyHolderDob)
                    .NotEmpty()
                    .WithMessage("Either the Policy Holder's DOB or their Email Address must be supplied.");

                RuleFor(m => m.PolicyHolderEmailAddress)
                    .NotEmpty()
                    .WithMessage("Either the Policy Holder's DOB or their Email Address must be supplied.");
            });

            When(m => m.PolicyHolderDob.HasValue, () =>
            {
                RuleFor(m => m.PolicyHolderDob)
                    .NotEmpty()
                    .Must(BeOverEighteen)
                    .WithMessage("Policy Holder must be 18 years old.");
            });

            When(m => !string.IsNullOrWhiteSpace(m.PolicyHolderEmailAddress), () =>
            {
                RuleFor(m => m.PolicyHolderEmailAddress)
                   .NotEmpty()
                   .Matches("^[a-zA-Z0-9]{4,}@[a-zA-Z0-9]{2,}(.com|.co.uk)$");
            });
        }

        private bool BeOverEighteen(DateTime? dob)
        {
            if(!dob.HasValue)
            {
                return false;
            }

            var eighteenYearsAgo = DateTime.Now.AddYears(-18);
            return dob.Value <= eighteenYearsAgo;
        }
    }
}