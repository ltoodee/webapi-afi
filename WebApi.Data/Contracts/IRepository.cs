﻿namespace WebApi.Data.Contracts
{
    public interface IRepository<T> where T : class
    {
        int Create(T entity);

        T Get(int id);

        T Update(int id, T entity);

        bool Delete(int id);
    }
}
