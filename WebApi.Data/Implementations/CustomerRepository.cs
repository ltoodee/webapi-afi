﻿using Microsoft.AspNetCore.Hosting;
using System.Collections.Generic;
using System.IO;
using System.Text.Json;
using WebApi.Core.Models;
using WebApi.Data.Contracts;
using WebApi.Data.Models;

namespace WebApi.Data.Implementations
{
    public class CustomerRepository : IRepository<CustomerModel>
    {
        private readonly IWebHostEnvironment _hostingEnvironment;
        private const string databaseName = "database.dat";

        public CustomerRepository(IWebHostEnvironment hostEnvironment)
        {
            _hostingEnvironment = hostEnvironment;
        }

        public int Create(CustomerModel customer)
        {
            var path = Path.Combine(_hostingEnvironment.ContentRootPath, databaseName);
            var latestId = 0;
            var entries = new List<CustomerDataModel>();
            if(File.Exists(path))
            {
                var contents = File.ReadAllText(path);
                if (!string.IsNullOrWhiteSpace(contents))
                {
                    entries = JsonSerializer.Deserialize<List<CustomerDataModel>>(contents);
                    latestId = entries[entries.Count - 1].Id;
                }
            }
            
            // TODO: Would rather use AutoMapper than mapping manually.
            var entry = new CustomerDataModel()
            {
                Id = GetNextId(latestId),
                PolicyHolderFirstName = customer.PolicyHolderFirstName,
                PolicyHolderLastName = customer.PolicyHolderLastName,
                PolicyHolderDob = customer.PolicyHolderDob,
                PolicyHolderEmailAddress = customer.PolicyHolderEmailAddress,
                PolicyHolderReferenceNumber = customer.PolicyHolderReferenceNumber
            };
            entries.Add(entry);

            File.WriteAllText(path, JsonSerializer.Serialize(entries));

            return entry.Id;
        }

        public bool Delete(int customerId)
        {
            throw new System.NotImplementedException();
        }

        public CustomerModel Get(int customerId)
        {
            throw new System.NotImplementedException();
        }

        public CustomerModel Update(int customerId, CustomerModel customer)
        {
            throw new System.NotImplementedException();
        }

        private static int GetNextId(int currentId)
        {
            return currentId == 0 ? 10000 : currentId + 1;
        }
    }
}
