﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WebApi.Data.Models
{
    public class CustomerDataModel
    {
        public int Id { get; set; }
        public string PolicyHolderFirstName { get; set; }
        public string PolicyHolderLastName { get; set; }
        public string PolicyHolderReferenceNumber { get; set; }
        public DateTime? PolicyHolderDob { get; set; }
        public string PolicyHolderEmailAddress { get; set; }
    }
}
