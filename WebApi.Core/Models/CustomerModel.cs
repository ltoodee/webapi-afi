﻿using System;

namespace WebApi.Core.Models
{
    /// <summary>
    /// Represents the Customer Model.
    /// Validation rules can be found in <see cref="Validators.CustomerModelValidator"/>.
    /// </summary>
    public class CustomerModel
    {
        public string PolicyHolderFirstName { get; set; }
        public string PolicyHolderLastName { get; set; }
        public string PolicyHolderReferenceNumber { get; set; }
        public DateTime? PolicyHolderDob { get; set; }
        public string PolicyHolderEmailAddress { get; set; }
    }
}
