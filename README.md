# Overview
This project contains a single API endpoint to create a customer and return the ID of the customer.

In terms of persistence, the records are stored in a file within the WebApi folder, called database.dat.

# How to run

## dotnet CLI
1. Clone repository.
2. Navigate to root folder in PowerShell/CMD prompt.
3. Run `dotnet test` to run the unit tests.
4. Run `dotnet run --project WebApi` to start the application.
5. Navigate to [RootURL]/swagger (usually https://localhost:5001/swagger) for SwaggerUI.

# API Tests
In the ApiTests folder in the root of the repository you'll find a Postman collection which will test the functionality of the CreateClient API endpoint.

1. Download Postman (https://www.postman.com/).
2. Import the collection.
3. Run the collection.
   ![Run the collection](ReadmeFiles/RunTheCollection.png)
   
# TODO list
- Add database and ORM (usually Entity Framework).
- Extend Unit tests.
- Extend API tests.
- Add logging capability.
- Add AutoMapper for mapping between model types.