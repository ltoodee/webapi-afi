﻿using FluentValidation.TestHelper;
using System;
using WebApi.Core.Models;
using WebApi.Validators;
using Xunit;

namespace WebApi.Core.Test.Validators
{
    public class CustomerModelValidatorTest
    {
        private readonly CustomerModelValidator _customerModelValidator;

        public CustomerModelValidatorTest()
        {
            _customerModelValidator = new CustomerModelValidator();
        }

        [Theory]
        [InlineData(null)] // Null
        [InlineData("")] // Empty
        [InlineData(" ")] // Empty
        [InlineData("A")] // Too short
        [InlineData("AA")] // Too short
        [InlineData("AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA")] // Too long (51 chars)
        public void FirstNameValidationFails(string firstName)
        {
            // ARRANGE
            var model = new CustomerModel()
            {
                PolicyHolderFirstName = firstName,
            };

            // ACT
            var result = _customerModelValidator.TestValidate(model);

            // ASSERT
            result.ShouldHaveValidationErrorFor(x => x.PolicyHolderFirstName);
        }

        [Theory]
        [InlineData("AAA")] // Lower boundary
        [InlineData("AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA")] // Upper boundary
        public void FirstNameValidationPasses(string firstName)
        {
            // ARRANGE
            var model = new CustomerModel()
            {
                PolicyHolderFirstName = firstName,
            };

            // ACT
            var result = _customerModelValidator.TestValidate(model);

            // ASSERT
            result.ShouldNotHaveValidationErrorFor(x => x.PolicyHolderFirstName);
        }

        [Fact]
        public void DobValidationPolicyHolderUnderageFails()
        {
            // ARRANGE
            var model = new CustomerModel()
            {
                PolicyHolderDob = DateTime.Now.AddYears(-18).AddDays(1)
            };

            // ACT
            var result = _customerModelValidator.TestValidate(model);

            // ASSERT
            result.ShouldHaveValidationErrorFor(x => x.PolicyHolderDob);
        }

        [Fact]
        public void DobValidationPolicyHolderOfAgePasses()
        {
            // ARRANGE
            var model = new CustomerModel()
            {
                PolicyHolderDob = DateTime.Now.AddYears(-18)
            };

            // ACT
            var result = _customerModelValidator.TestValidate(model);

            // ASSERT
            result.ShouldNotHaveValidationErrorFor(x => x.PolicyHolderDob);
        }

        // TODO: Write more tests to cover the validation requirements of the Customer Model
    }
}
